package info.globalbus.JakaGmina.gdal;

import org.gdal.ogr.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by globalbus on 07.02.16.
 */
public class GdalFacade {
    static {
        ogr.RegisterAll();
        ogr.UseExceptions();
    }

    public List<String> fileToGeoJson(String absolutePath, int geometryType) {
        DataSource dataSource=null;
        try {
            List<String> geojsons = new ArrayList<>();
            dataSource = ogr.Open(absolutePath);
            for (int i = 0; i < dataSource.GetLayerCount(); i++) {
                Layer layer = dataSource.GetLayer(i);
                if (layer.GetFeatureCount() == 0)
                    continue;
                while (true) {
                    Feature feature = layer.GetNextFeature();
                    if (feature == null)
                        break;
                    Geometry geometry = feature.GetGeometryRef();
                    if (feature.GetDefnRef().GetGeomType() == geometryType)
                        geojsons.add(geometry.ExportToJson());


                }
            }
            return geojsons;
        }
        finally{
            if(dataSource!=null)
                dataSource.delete();
        }
    }
    public List<String> getFeatureNames(String absolutePath, int geometryType) {
        DataSource dataSource=null;
        try {
            List<String> geojsons = new ArrayList<>();
            dataSource = ogr.Open(absolutePath);
            for (int i = 0; i < dataSource.GetLayerCount(); i++) {
                Layer layer = dataSource.GetLayer(i);
                if (layer.GetFeatureCount() == 0)
                    continue;
                while (true) {
                    Feature feature = layer.GetNextFeature();
                    if (feature == null)
                        break;
                    Geometry geometry = feature.GetGeometryRef();
                    if (feature.GetDefnRef().GetGeomType() == geometryType)
                        geojsons.add(feature.GetFieldAsString("name"));

                }
            }
            return geojsons;
        }
        finally{
            if(dataSource!=null)
                dataSource.delete();
        }
    }
}
