package info.globalbus.JakaGmina.Dto;

import info.globalbus.JakaGmina.Dto.geojson.GeoJsonMultiPolygon;
import lombok.EqualsAndHashCode;
import lombok.Value;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by globalbus on 07.02.16.
 */
@Value
@Document(collection = "gminy_simply")
@EqualsAndHashCode(callSuper=true)
public class GminaSimply extends Gmina {
    private String type;
    private GeoJsonMultiPolygon geometry;
}
