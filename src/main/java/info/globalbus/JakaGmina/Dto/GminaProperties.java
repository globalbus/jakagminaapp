package info.globalbus.JakaGmina.Dto;

/**
 * Created by globalbus on 31.01.16.
 */

import lombok.Value;
import org.springframework.data.mongodb.core.mapping.Field;

@Value
public class GminaProperties {
    @Field("jpt_nazwa_")
    private String name;
    @Field("jpt_kod_je")
    private Integer id;
}
