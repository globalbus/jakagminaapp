package info.globalbus.JakaGmina.Dto;

import lombok.*;
import org.springframework.data.mongodb.core.geo.GeoJson;

import java.util.Collection;
import java.util.List;

/**
 * Created by globalbus on 07.02.16.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TrackQueryResult {
    Collection<Integer> gminyId;
    GeoJson<?> geoJson;
    String trackName;

    public TrackQueryResult(List<Integer> ids, GeoJson template) {
        this.gminyId=ids;
        this.geoJson=template;
    }
}
