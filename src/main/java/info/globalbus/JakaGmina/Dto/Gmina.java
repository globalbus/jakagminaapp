package info.globalbus.JakaGmina.Dto;

import lombok.Getter;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by globalbus on 31.01.16.
 */
@Getter
@Document(collection = "gminy")
public class Gmina {
    protected GminaProperties properties;
}
