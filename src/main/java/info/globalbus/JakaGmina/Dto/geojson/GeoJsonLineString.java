package info.globalbus.JakaGmina.Dto.geojson;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.geo.GeoJson;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by globalbus on 07.02.16.
 */
@Data
@NoArgsConstructor
public class GeoJsonLineString implements GeoJson<Iterable<GeoJsonPoint>> {
    private final String type = "LineString";
    List<GeoJsonPoint> coordinates;

    public GeoJsonLineString(List<GeoJsonPoint> coordinates) {
        this.coordinates=coordinates;
    }


    public void setCoordinates(List<List<Double>> value) {
        coordinates = new ArrayList<>();
        for (List<Double> it:value) {
            GeoJsonPoint lineString = new GeoJsonPoint();
            lineString.setCoordinates(it);
            coordinates.add(lineString);
        }
    }
}
