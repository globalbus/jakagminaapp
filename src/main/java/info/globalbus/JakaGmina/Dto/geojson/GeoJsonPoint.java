package info.globalbus.JakaGmina.Dto.geojson;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.geo.GeoJson;

import java.util.Iterator;
import java.util.List;

/**
 * Created by globalbus on 07.02.16.
 */
@Data
@NoArgsConstructor
public class GeoJsonPoint implements GeoJson<Iterable<Double>> {
    private final String type = "Point";
    PointTuple coordinates;

    public GeoJsonPoint(double x, double y) {
        coordinates=new PointTuple(x,y);
    }
    public void setCoordinates(List<Double> list){
        coordinates= new PointTuple(list.get(0),list.get(1));
    }
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public class PointTuple implements Iterable<Double>{
        Double x;
        Double y;

        @Override
        public Iterator<Double> iterator() {
            return new Iterator<Double>() {
                int index=0;
                @Override
                public boolean hasNext() {
                    if(index<2)
                        return true;
                    return false;
                }

                @Override
                public Double next() {
                    index++;
                    if(index==1)
                        return x;
                    else if (index==2)
                        return y;
                    return null;
                }
            };
        }
    }
}
