package info.globalbus.JakaGmina.Dto.geojson;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.geo.GeoJson;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by globalbus on 14.02.16.
 */
@Data
@NoArgsConstructor
public class GeoJsonMultiPolygon implements GeoJson<List<GeoJsonPolygon>> {
    private final String type = "MultiPolygon";
    List<GeoJsonPolygon> coordinates;

    public GeoJsonMultiPolygon(List<GeoJsonPolygon> coordinates) {
        this.coordinates=coordinates;
    }

    public void setCoordinates(List<List<List<List<Double>>>> value){
        coordinates = new ArrayList<GeoJsonPolygon>();
        for (List<List<List<Double>>> it:value) {
            GeoJsonPolygon lineString = new GeoJsonPolygon();
            lineString.setCoordinates(it);
            coordinates.add(lineString);
        }
    }
}
