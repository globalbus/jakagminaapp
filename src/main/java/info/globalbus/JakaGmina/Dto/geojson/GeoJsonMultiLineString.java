package info.globalbus.JakaGmina.Dto.geojson;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.geo.GeoJson;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by globalbus on 07.02.16.
 */
@Data
@NoArgsConstructor
public class GeoJsonMultiLineString implements GeoJson<Iterable<GeoJsonLineString>>{
    private final String type = "MultiLineString";
    List<GeoJsonLineString> coordinates = new ArrayList<>();

    public GeoJsonMultiLineString(List<GeoJsonLineString> coordinates) {
        this.coordinates=coordinates;
    }

    public void setCoordinates(List<List<List<Double>>> value){
        coordinates = new ArrayList<GeoJsonLineString>();
        for (List<List<Double>> it:value) {
            GeoJsonLineString lineString = new GeoJsonLineString();
            lineString.setCoordinates(it);
            coordinates.add(lineString);
        }

    }
}
