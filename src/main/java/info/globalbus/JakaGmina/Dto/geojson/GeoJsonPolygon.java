package info.globalbus.JakaGmina.Dto.geojson;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.geo.GeoJson;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by globalbus on 14.02.16.
 */
@Data
@NoArgsConstructor
public class GeoJsonPolygon implements GeoJson<List<GeoJsonLineString>> {
    private final String type = "Polygon";
    List<GeoJsonLineString> coordinates;

    public GeoJsonPolygon(List<GeoJsonPoint> points) {
        this.coordinates = new ArrayList<>();
        this.coordinates.add(new GeoJsonLineString(points));
    }

    public void setCoordinates(List<List<List<Double>>> value){
        coordinates = new ArrayList<GeoJsonLineString>();
        for (List<List<Double>> it:value) {
            GeoJsonLineString lineString = new GeoJsonLineString();
            lineString.setCoordinates(it);
            coordinates.add(lineString);
        }
    }
}
