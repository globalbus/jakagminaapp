package info.globalbus.JakaGmina;

import info.globalbus.JakaGmina.Dto.Gmina;
import info.globalbus.JakaGmina.Dto.GminaSimply;
import info.globalbus.JakaGmina.mongo.GminaRepository;
import info.globalbus.JakaGmina.mongo.GminaSimplyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

/**
 * Created by globalbus on 31.01.16.
 */
@RequestMapping("/ajax")
@RestController
public class AjaxController {
    @Autowired
    GminaSimplyRepository gminaSimplyRepository;
    @Autowired
    GminaRepository gminaRepository;
    @RequestMapping("/all")
    public Collection<GminaSimply> getAll(){
        return gminaSimplyRepository.findAll();
    }
    @RequestMapping("/where")
    public Collection<Gmina> getWhere(double x, double y){
        return gminaRepository.findWithin(new GeoJsonPoint(x, y));
    }
    @RequestMapping("/track")
    public Collection<GminaSimply> getInterceptions(@RequestBody String track){
        return null;//TODO do redirect
    }

}
