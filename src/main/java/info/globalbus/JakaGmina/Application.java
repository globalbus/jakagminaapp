package info.globalbus.JakaGmina;

import info.globalbus.JakaGmina.gdal.GdalFacade;
import nz.net.ultraq.thymeleaf.LayoutDialect;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan
@EnableAutoConfiguration
public class Application extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {

        return application.sources(Application.class);
    }
    @Bean
    LayoutDialect layoutDialect(){
        return new LayoutDialect();
    }
    @Bean
    GdalFacade gdalFacade() {return new GdalFacade();}

}
