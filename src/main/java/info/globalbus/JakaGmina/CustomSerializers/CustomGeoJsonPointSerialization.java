package info.globalbus.JakaGmina.CustomSerializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import info.globalbus.JakaGmina.Dto.geojson.GeoJsonPoint;

import java.io.IOException;

/**
 * Created by globalbus on 31.01.16.
 */
public class CustomGeoJsonPointSerialization extends StdSerializer<GeoJsonPoint>{

        public CustomGeoJsonPointSerialization() {
            super(GeoJsonPoint.class);
        }

    @Override
    public void serialize(GeoJsonPoint value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeStartArray();
        gen.writeNumber(value.getCoordinates().getX().floatValue());
        gen.writeNumber(value.getCoordinates().getY().floatValue());
        gen.writeEndArray();
    }
}
