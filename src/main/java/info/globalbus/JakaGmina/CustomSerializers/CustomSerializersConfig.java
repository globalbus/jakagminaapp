package info.globalbus.JakaGmina.CustomSerializers;

import info.globalbus.JakaGmina.Dto.geojson.GeoJsonLineString;
import info.globalbus.JakaGmina.Dto.geojson.GeoJsonPoint;
import info.globalbus.JakaGmina.Dto.geojson.GeoJsonPolygon;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

/**
 * Created by globalbus on 31.01.16.
 */
@Configuration
public class CustomSerializersConfig {
    @Bean
    public Jackson2ObjectMapperBuilder jacksonBuilder() {
        Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
        builder.serializerByType(GeoJsonPoint.class, new CustomGeoJsonPointSerialization());
        builder.serializerByType(GeoJsonPolygon.class, new CustomGeoJsonPolygonSerialization());
        builder.serializerByType(GeoJsonLineString.class, new CustomGeoJsonLineStringSerialization());
        return builder;
    }
}
