package info.globalbus.JakaGmina.CustomSerializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import info.globalbus.JakaGmina.Dto.geojson.GeoJsonPolygon;

import java.io.IOException;

/**
 * Created by globalbus on 31.01.16.
 */
public class CustomGeoJsonPolygonSerialization extends StdSerializer<GeoJsonPolygon> {

    public CustomGeoJsonPolygonSerialization() {
        super(GeoJsonPolygon.class);
    }

    @Override
    public void serialize(GeoJsonPolygon value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeObject(value.getCoordinates());
    }
}