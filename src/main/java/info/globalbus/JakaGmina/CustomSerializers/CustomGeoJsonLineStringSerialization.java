package info.globalbus.JakaGmina.CustomSerializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import info.globalbus.JakaGmina.Dto.geojson.GeoJsonLineString;

import java.io.IOException;

public class CustomGeoJsonLineStringSerialization extends StdSerializer<GeoJsonLineString> {

    public CustomGeoJsonLineStringSerialization() {
        super(GeoJsonLineString.class);
    }

    @Override
    public void serialize(GeoJsonLineString value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeObject(value.getCoordinates());
    }
}
