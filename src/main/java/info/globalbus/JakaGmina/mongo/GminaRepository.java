package info.globalbus.JakaGmina.mongo;

import info.globalbus.JakaGmina.Dto.Gmina;
import org.springframework.data.mongodb.core.geo.GeoJson;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

/**
 * Created by globalbus on 31.01.16.
 */

public interface GminaRepository extends MongoRepository<Gmina, String> {
    @Query("{'geometry':{$geoIntersects:{$geometry:?0}}}")
    List<Gmina> findWithin(GeoJson point);
}
