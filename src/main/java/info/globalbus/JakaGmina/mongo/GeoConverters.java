/*
 * Copyright 2014-2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package info.globalbus.JakaGmina.mongo;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import info.globalbus.JakaGmina.Dto.geojson.*;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.core.convert.CustomConversions;
import org.springframework.data.mongodb.core.geo.GeoJson;
import org.springframework.data.mongodb.core.geo.GeoJsonGeometryCollection;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
@Component
public class GeoConverters extends CustomConversions {

	public GeoConverters() {
		super(getConvertersToRegister());
	}

	public static List<? extends Object> getConvertersToRegister() {
		return Arrays.asList( //
				 GeoJsonToDbObjectConverter.INSTANCE //
				, GeoJsonPointToDbObjectConverter.INSTANCE //
				, GeoJsonPolygonToDbObjectConverter.INSTANCE //
				, DbObjectToGeoJsonPointConverter.INSTANCE //
				, DbObjectToGeoJsonPolygonConverter.INSTANCE //
				, DbObjectToGeoJsonLineStringConverter.INSTANCE //
				, DbObjectToGeoJsonMultiLineStringConverter.INSTANCE //
				, DbObjectToGeoJsonMultiPointConverter.INSTANCE //
				, DbObjectToGeoJsonMultiPolygonConverter.INSTANCE //
				, DbObjectToGeoJsonGeometryCollectionConverter.INSTANCE);
	}


	/**
	 * @author Christoph Strobl
	 * @since 1.7
	 */
	@SuppressWarnings("rawtypes")
	static enum GeoJsonToDbObjectConverter implements Converter<GeoJson, DBObject> {

		INSTANCE;

		/*
		 * (non-Javadoc)
		 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
		 */
		@Override
		public DBObject convert(GeoJson source) {

			if (source == null) {
				return null;
			}

			DBObject dbo = new BasicDBObject("type", source.getType());

			if (source instanceof GeoJsonGeometryCollection) {

				BasicDBList dbl = new BasicDBList();

				for (GeoJson geometry : ((GeoJsonGeometryCollection) source).getCoordinates()) {
					dbl.add(convert(geometry));
				}

				dbo.put("geometries", dbl);

			} else {
				dbo.put("coordinates", convertIfNecessarry(source.getCoordinates()));
			}

			return dbo;
		}

		private Object convertIfNecessarry(Object candidate) {

			if (candidate instanceof GeoJson) {
				return convertIfNecessarry(((GeoJson) candidate).getCoordinates());
			}

			if (candidate instanceof Iterable) {

				BasicDBList dbl = new BasicDBList();

				for (Object element : (Iterable) candidate) {
					dbl.add(convertIfNecessarry(element));
				}

				return dbl;
			}

			if (candidate instanceof Point) {
				return toList((Point) candidate);
			}

			return candidate;
		}
	}

	/**
	 * @author Christoph Strobl
	 * @since 1.7
	 */
	static enum GeoJsonPointToDbObjectConverter implements Converter<GeoJsonPoint, DBObject> {

		INSTANCE;

		/*
		 * (non-Javadoc)
		 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
		 */
		@Override
		public DBObject convert(GeoJsonPoint source) {
			return GeoJsonToDbObjectConverter.INSTANCE.convert(source);
		}
	}

	/**
	 * @author Christoph Strobl
	 * @since 1.7
	 */
	static enum GeoJsonPolygonToDbObjectConverter implements Converter<GeoJsonPolygon, DBObject> {

		INSTANCE;

		/*
		 * (non-Javadoc)
		 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
		 */
		@Override
		public DBObject convert(GeoJsonPolygon source) {
			return GeoJsonToDbObjectConverter.INSTANCE.convert(source);
		}
	}

	/**
	 * @author Christoph Strobl
	 * @since 1.7
	 */
	static enum DbObjectToGeoJsonPointConverter implements Converter<DBObject, GeoJsonPoint> {

		INSTANCE;

		/*
		 * (non-Javadoc)
		 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
		 */
		@Override
		@SuppressWarnings("unchecked")
		public GeoJsonPoint convert(DBObject source) {

			if (source == null) {
				return null;
			}

			Assert.isTrue(ObjectUtils.nullSafeEquals(source.get("type"), "Point"),
					String.format("Cannot convert type '%s' to Point.", source.get("type")));

			List<Double> dbl = (List<Double>) source.get("coordinates");
			return new GeoJsonPoint(dbl.get(0).doubleValue(), dbl.get(1).doubleValue());
		}
	}

	/**
	 * @author Christoph Strobl
	 * @since 1.7
	 */
	static enum DbObjectToGeoJsonPolygonConverter implements Converter<DBObject, GeoJsonPolygon> {

		INSTANCE;

		/*
		 * (non-Javadoc)
		 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
		 */
		@Override
		public GeoJsonPolygon convert(DBObject source) {

			if (source == null) {
				return null;
			}

			Assert.isTrue(ObjectUtils.nullSafeEquals(source.get("type"), "Polygon"),
					String.format("Cannot convert type '%s' to Polygon.", source.get("type")));

			return toGeoJsonPolygon((BasicDBList) source.get("coordinates"));
		}
	}

	/**
	 * @author Christoph Strobl
	 * @since 1.7
	 */
	static enum DbObjectToGeoJsonMultiPolygonConverter implements Converter<DBObject, GeoJsonMultiPolygon> {

		INSTANCE;

		/*
		 * (non-Javadoc)
		 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
		 */
		@Override
		public GeoJsonMultiPolygon convert(DBObject source) {

			if (source == null) {
				return null;
			}

			Assert.isTrue(ObjectUtils.nullSafeEquals(source.get("type"), "MultiPolygon"),
					String.format("Cannot convert type '%s' to MultiPolygon.", source.get("type")));

			BasicDBList dbl = (BasicDBList) source.get("coordinates");
			List<GeoJsonPolygon> polygones = new ArrayList<GeoJsonPolygon>();

			for (Object polygon : dbl) {
				polygones.add(toGeoJsonPolygon((BasicDBList) polygon));
			}

			return new GeoJsonMultiPolygon(polygones);
		}
	}

	/**
	 * @author Christoph Strobl
	 * @since 1.7
	 */
	static enum DbObjectToGeoJsonLineStringConverter implements Converter<DBObject, GeoJsonLineString> {

		INSTANCE;

		/*
		 * (non-Javadoc)
		 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
		 */
		@Override
		public GeoJsonLineString convert(DBObject source) {

			if (source == null) {
				return null;
			}

			Assert.isTrue(ObjectUtils.nullSafeEquals(source.get("type"), "LineString"),
					String.format("Cannot convert type '%s' to LineString.", source.get("type")));

			BasicDBList cords = (BasicDBList) source.get("coordinates");

			return new GeoJsonLineString(toListOfPoint(cords));
		}
	}

	/**
	 * @author Christoph Strobl
	 * @since 1.7
	 */
	static enum DbObjectToGeoJsonMultiPointConverter implements Converter<DBObject, GeoJsonMultiPoint> {

		INSTANCE;

		/*
		 * (non-Javadoc)
		 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
		 */
		@Override
		public GeoJsonMultiPoint convert(DBObject source) {

			if (source == null) {
				return null;
			}

			Assert.isTrue(ObjectUtils.nullSafeEquals(source.get("type"), "MultiPoint"),
					String.format("Cannot convert type '%s' to MultiPoint.", source.get("type")));

			BasicDBList cords = (BasicDBList) source.get("coordinates");

			return new GeoJsonMultiPoint(toListOfPoint(cords));
		}
	}

	/**
	 * @author Christoph Strobl
	 * @since 1.7
	 */
	static enum DbObjectToGeoJsonMultiLineStringConverter implements Converter<DBObject, GeoJsonMultiLineString> {

		INSTANCE;

		/*
		 * (non-Javadoc)
		 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
		 */
		@Override
		public GeoJsonMultiLineString convert(DBObject source) {

			if (source == null) {
				return null;
			}

			Assert.isTrue(ObjectUtils.nullSafeEquals(source.get("type"), "MultiLineString"),
					String.format("Cannot convert type '%s' to MultiLineString.", source.get("type")));

			List<GeoJsonLineString> lines = new ArrayList<GeoJsonLineString>();
			BasicDBList cords = (BasicDBList) source.get("coordinates");

			for (Object line : cords) {
				lines.add(new GeoJsonLineString(toListOfPoint((BasicDBList) line)));
			}
			return new GeoJsonMultiLineString(lines);
		}
	}

	/**
	 * @author Christoph Strobl
	 * @since 1.7
	 */
	static enum DbObjectToGeoJsonGeometryCollectionConverter implements Converter<DBObject, GeoJsonGeometryCollection> {

		INSTANCE;

		/*
		 * (non-Javadoc)
		 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
		 */
		@SuppressWarnings("rawtypes")
		@Override
		public GeoJsonGeometryCollection convert(DBObject source) {

			if (source == null) {
				return null;
			}

			Assert.isTrue(ObjectUtils.nullSafeEquals(source.get("type"), "GeometryCollection"),
					String.format("Cannot convert type '%s' to GeometryCollection.", source.get("type")));

			List<GeoJson<?>> geometries = new ArrayList<GeoJson<?>>();
			for (Object o : (List) source.get("geometries")) {
				geometries.add(convertGeometries((DBObject) o));
			}
			return new GeoJsonGeometryCollection(geometries);

		}

		private static GeoJson<?> convertGeometries(DBObject source) {

			Object type = source.get("type");
			if (ObjectUtils.nullSafeEquals(type, "Point")) {
				return DbObjectToGeoJsonPointConverter.INSTANCE.convert(source);
			}

			if (ObjectUtils.nullSafeEquals(type, "MultiPoint")) {
				return DbObjectToGeoJsonMultiPointConverter.INSTANCE.convert(source);
			}

			if (ObjectUtils.nullSafeEquals(type, "LineString")) {
				return DbObjectToGeoJsonLineStringConverter.INSTANCE.convert(source);
			}

			if (ObjectUtils.nullSafeEquals(type, "MultiLineString")) {
				return DbObjectToGeoJsonMultiLineStringConverter.INSTANCE.convert(source);
			}

			if (ObjectUtils.nullSafeEquals(type, "Polygon")) {
				return DbObjectToGeoJsonPolygonConverter.INSTANCE.convert(source);
			}
			if (ObjectUtils.nullSafeEquals(type, "MultiPolygon")) {
				return DbObjectToGeoJsonMultiPolygonConverter.INSTANCE.convert(source);
			}

			throw new IllegalArgumentException(String.format("Cannot convert unknown GeoJson type %s", type));
		}
	}

	static List<Double> toList(Point point) {
		return Arrays.asList(point.getX(), point.getY());
	}

	/**
	 * Converts a coordinate pairs nested in in {@link BasicDBList} into {@link GeoJsonPoint}s.
	 * 
	 * @param listOfCoordinatePairs
	 * @return
	 * @since 1.7
	 */
	@SuppressWarnings("unchecked")
	static List<GeoJsonPoint> toListOfPoint(BasicDBList listOfCoordinatePairs) {

		List<GeoJsonPoint> points = new ArrayList<GeoJsonPoint>();

		for (Object point : listOfCoordinatePairs) {

			Assert.isInstanceOf(List.class, point);

			List<Double> coordinatesList = (List<Double>) point;

			points.add(new GeoJsonPoint(coordinatesList.get(0).doubleValue(), coordinatesList.get(1).doubleValue()));
		}
		return points;
	}

	/**
	 * Converts a coordinate pairs nested in in {@link BasicDBList} into {@link GeoJsonPolygon}.
	 * 
	 * @param dbList
	 * @return
	 * @since 1.7
	 */
	static GeoJsonPolygon toGeoJsonPolygon(BasicDBList dbList) {
		return new GeoJsonPolygon(toListOfPoint((BasicDBList) dbList.get(0)));
	}
}
