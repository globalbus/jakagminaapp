package info.globalbus.JakaGmina.mongo;

import info.globalbus.JakaGmina.Dto.GminaSimply;
import org.springframework.data.mongodb.core.geo.GeoJson;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

/**
 * Created by globalbus on 07.02.16.
 */
public interface GminaSimplyRepository extends MongoRepository<GminaSimply, String> {
    @Query("{'geometry':{$geoIntersects:{$geometry:?0}}}")
    List<GminaSimply> findWithin(GeoJson point);
}
