package info.globalbus.JakaGmina;

import com.fasterxml.jackson.databind.ObjectMapper;
import info.globalbus.JakaGmina.Dto.Gmina;
import info.globalbus.JakaGmina.Dto.TrackQueryResult;
import info.globalbus.JakaGmina.Dto.geojson.GeoJsonMultiLineString;
import info.globalbus.JakaGmina.gdal.GdalFacade;
import info.globalbus.JakaGmina.mongo.GminaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.geo.GeoJson;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import static org.gdal.ogr.ogrConstants.wkbMultiLineString;

/**
 * Created by globalbus on 05.02.16.
 */
@RestController
@RequestMapping("/upload")
public class UploadController {
    public static final String GPX_MIME_TYPE="application/gpx+xml";
    @Autowired
    GminaRepository repo;
    @Autowired
    GdalFacade gdalFacade;
    ObjectMapper mapper = new ObjectMapper();
    @RequestMapping(value = "/track")
    public ResponseEntity<?> uploadTrack(MultipartFile trackFile) {
        if (!(trackFile == null && trackFile.isEmpty())) {
            UUID id = UUID.randomUUID();
            File realFile = null;
            try {
                realFile = File.createTempFile(id.toString(), null);
                File file = new File(realFile.getName());
                trackFile.transferTo(file);
                List<String> geoJsons = gdalFacade.fileToGeoJson(realFile.getAbsolutePath(), wkbMultiLineString);
                List<String> trackNames = gdalFacade.getFeatureNames(realFile.getAbsolutePath(), wkbMultiLineString);
                if(geoJsons.size()!=trackNames.size())
                    throw new Exception("track info non consistent");
                List<TrackQueryResult> results = new LinkedList<>();
                for (int i=0; i<geoJsons.size(); i++) {
                    GeoJsonMultiLineString geoJsonMultiLineString = new GeoJsonMultiLineString();
                    geoJsonMultiLineString.getCoordinates().addAll(mapper.readValue(geoJsons.get(i), GeoJsonMultiLineString.class).getCoordinates());
                    TrackQueryResult result = getInterceptions(geoJsonMultiLineString);
                    result.setTrackName(trackNames.get(i));
                    results.add(result);
                }
                return new ResponseEntity<>(results, HttpStatus.OK);
            } catch (Exception e) {
                return new ResponseEntity<>("Error on conversion", HttpStatus.INTERNAL_SERVER_ERROR);
            } finally {
                if (realFile != null && realFile.exists())
                    realFile.delete();
            }
        }
        return new ResponseEntity<>("No file was given", HttpStatus.BAD_REQUEST);
    }

    public TrackQueryResult getInterceptions(GeoJson template) {
        List<Gmina> result = repo.findWithin(template);
        List<Integer> ids = new LinkedList<>();
        for (Gmina g : result) {
            ids.add(g.getProperties().getId());
        }

        return new TrackQueryResult(ids, template);
    }

}
