 var gminyPolygons = new Object();
 var tracks = new Array();
 var map;
 $(function(event) {
    $('#loadButton').click(loadTrack);
        var mapquest = L.tileLayer('http://otile{s}.mqcdn.com/tiles/1.0.0/{type}/{z}/{x}/{y}.png', {
            subdomains: '1234',
            type: 'osm',
            attribution: 'Tiles &copy; <a href="http://www.mapquest.com/" target="_blank">MapQuest</a> <img src="http://developer.mapquest.com/content/osm/mq_logo.png" />'
        });
    map= L.map('map', {zoomControl:false, layers: [mapquest]}).setView([51.505, 21.09], 6);

    $.ajax("/ajax/all").done(function(data) {
        for(var i=0;i<data.length;i++){
            var el=data[i];
            gminyPolygons[el.properties.id]=new L.GeoJSON(el, { onEachFeature: onEachFeature});
            gminyPolygons[el.properties.id]['name']=el.properties.name;
            gminyPolygons[el.properties.id]['selected']=false;
            gminyPolygons[el.properties.id].addTo(map);
        }});
    });
    var defaultStyle = {
            color: "#e88127",
            weight: 1,
            opacity: 0.6,
            fillOpacity: 0.3,
            fillColor: "#e88127"
    };

    var visitedStyle = {
            color: "#93a432",
            weight: 1,
            opacity: 0.6,
            fillOpacity: 0.5,
            fillColor: "#93a432"
    };
function onEachFeature(feature, layer) {
    if (feature.properties) {
        layer.bindPopup(feature.properties.name);
        layer.setStyle(defaultStyle);
    }
}
function loadTrack(){
    var fileInput =document.getElementById('file');
    var files = fileInput.files;
    if(files===undefined || files.length==0)
    {
        alert("No file specified");
        return;
    }
    for(var k=0;k<files.length;k++){
    var file = files[k];
    var form = new FormData();
    form.append("trackFile", file);
    ajaxHeaders = {
		method : "POST",
    contentType: false,
    processData: false,
		data: form
	};
    $.ajax("/upload/track", ajaxHeaders).done(function(trackInfo) {
        var root = $('#trackList');
        root.empty();
        $('<th><td>Id gminy</td><td>Nazwa gminy</td></th>').appendTo(root);
        for(var i=0;i<trackInfo.length;i++){
            var data =trackInfo[i];
            for(var j=0;j<data.gminyId.length;j++){
                var el = data.gminyId[j];
                gminyPolygons[el].selected=true;
                gminyPolygons[el].setStyle(visitedStyle);
            }
            var track = new L.GeoJSON(data.geoJson);
            tracks.push(track);
            track.addTo(map);
        }
        for (var key in gminyPolygons) {
            var el = gminyPolygons[key];
            if(el.selected==true){
                var elHtml = $("<tr></tr>");
                $("<td></td>", {text: key}).appendTo(elHtml);
                $("<td></td>", {text: el.name}).appendTo(elHtml);
                elHtml.appendTo(root);
            }
        }

     });
    }

}